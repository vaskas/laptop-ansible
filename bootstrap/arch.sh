#!/bin/sh

sudo sed --in-place 's/^#\s*\(%wheel\s\+ALL=(ALL)\s\+NOPASSWD:\s\+ALL\)/\1/' /etc/sudoers

sudo pacman -Suy
sudo pacman -S ansible base-devel

# AUR helper
git clone https://aur.archlinux.org/yay.git /tmp/yay
pushd /tmp/yay
makepkg -si
popd

# Ansible AUR plugin
git clone https://github.com/kewlfft/ansible-aur.git ~/.ansible/plugins/modules/aur

pushd src/st
make
sudo make install
popd

rm -rf ~/.bash* ~/.screen* ~/.xin* ~/.inputrc
