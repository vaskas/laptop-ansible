#!/bin/sh

sudo sed --in-place 's/^#\s*\(%sudo\s\+ALL=(ALL)\s\+NOPASSWD:\s\+ALL\)/\1/' /etc/sudoers

sudo apt update
sudo apt upgrade -y
sudo apt install -y ansible git

rm -rf ~/.bash* ~/.profile ~/.vim*
