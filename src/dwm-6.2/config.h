/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 4;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const unsigned int gappx     = 4;        /* gaps between windows */
static const char *fonts[]          = { "monospace:size=10" };
static const int topbar             = 1;        /* 0 means bottom bar */
static const int showbar            = 0;        /* 0 means no bar */

static const char col_gray1[]       = "#222222";
static const char col_gray2[]       = "#444444";
static const char col_gray3[]       = "#bbbbbb";
static const char col_gray4[]       = "#eeeeee";
static const char col_cyan[]        = "#005577";

static const char col_normal[]      = "#C0C0C0";
static const char col_active[]      = "#586e75";

static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray3, col_gray1, col_normal },
	[SchemeSel]  = { col_gray4, col_cyan,  col_active },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     isfloating   monitor */
	{ "Gimp",     NULL,       NULL,       0,            1,           -1 },
	{ "Firefox",  NULL,       NULL,       1 << 8,       0,           -1 },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[M]",      monocle },
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
};

/* key definitions */
#define MODKEY Mod1Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* dmenu stuff, needed by dwm.c */
static const char dmenufont[]       = "monospace:size=10";
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray4, NULL };

/* commands */
static const char *firefox[] = { "firefox", NULL, NULL, NULL, "firefox" };
static const char *telegram[] = { "telegram-desktop", NULL, NULL, NULL, "TelegramDesktop" };
static const char *nautilus[] = { "nautilus", NULL, NULL, NULL, "Org.gnome.Nautilus" };
static const char *mousepad[] = { "mousepad", NULL, NULL, NULL, "Mousepad" };

static Key keys[] = {
	/* modifier                     key        function        argument */

// Switch between apps
  { MODKEY,                       XK_f,      runorraise,     {.v = firefox } },
  { MODKEY,                       XK_g,      runorraise,     {.v = telegram } },
  { MODKEY,                       XK_n,      runorraise,     {.v = nautilus } },
  { MODKEY,                       XK_m,      runorraise,     {.v = mousepad } },

// Kill
	{ MODKEY,                       XK_w,      killclient,     {0} },

// Focus
	{ MODKEY,                       XK_Right,  focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_Left,   focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_Down,   focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_Up,     focusstack,     {.i = -1 } },

// Move windows
	{ MODKEY|ShiftMask,             XK_Left,   zoom,           {0} },
	{ MODKEY|ShiftMask,             XK_Right,  zoom,           {0} },

// Resize windows
	{ MODKEY|ControlMask,           XK_Left,   setmfact,       {.f = -0.05} },
	{ MODKEY|ControlMask,           XK_Right,  setmfact,       {.f = +0.05} },

// Float, tile or monocle (default)
  { MODKEY,                       XK_comma,  setlayout,      {.v = &layouts[2]} },
  { MODKEY,                       XK_s,      setlayout,      {.v = &layouts[1]} },
  { MODKEY,                       XK_q,      setlayout,      {.v = &layouts[0]} },

// Switch between tags
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)

// dwm actions
	{ MODKEY|ControlMask,           XK_q,      quit,           {0} },

// WTF are these?
	{ MODKEY,                       XK_space,  view,           {0} },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },

	//{ MODKEY,                       XK_s,      incnmaster,     {.i = +1 } },
	//{ MODKEY|ShiftMask,             XK_s,      incnmaster,     {.i = -1 } },
	//{ MODKEY,                       XK_comma,  togglefloating, {0} },
	//{ MODKEY,                       XK_b,      togglebar,      {0} },
	//{ MODKEY,                       XK_space,  setlayout,      {0} },
	//{ MODKEY,                       XK_Return, zoom,           {0} },
	//{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	//{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	//{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	//{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

