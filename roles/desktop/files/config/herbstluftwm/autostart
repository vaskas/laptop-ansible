#!/usr/bin/env bash

hc() {
    herbstclient "$@"
}

hc emit_hook reload

# remove all existing keybindings
hc keyunbind --all

# keybindings
Mod=Mod1

hc keybind Control-Shift-q quit
hc keybind Control-Shift-r reload

hc keybind $Mod-w close

# focusing clients
hc keybind $Mod-Left  focus left
hc keybind $Mod-Down  focus down
hc keybind $Mod-Up    focus up
hc keybind $Mod-Right focus right

# moving clients
hc keybind $Mod-Shift-Left  shift left
hc keybind $Mod-Shift-Down  shift down
hc keybind $Mod-Shift-Up    shift up
hc keybind $Mod-Shift-Right shift right

# splitting frames
hc keybind $Mod-s spawn hl-splitfocus.sh right right
hc keybind $Mod-Shift-s spawn hl-splitfocus.sh bottom down

# let the current frame explode into subframes
hc keybind $Mod-Control-space split explode

# resizing frames
resizestep=0.05
hc keybind $Mod-Control-Left    resize left +$resizestep
hc keybind $Mod-Control-Down    resize down +$resizestep
hc keybind $Mod-Control-Up      resize up +$resizestep
hc keybind $Mod-Control-Right   resize right +$resizestep

# default layout
hc set default_frame_layout 2
hc set_layout max

# tags
tag_names=( {1..3} )
tag_keys=( {1..3} 0 )

hc rename default "${tag_names[0]}" || true
for i in ${!tag_names[@]} ; do
    hc add "${tag_names[$i]}"
    key="${tag_keys[$i]}"
    if ! [ -z "$key" ] ; then
        hc keybind "$Mod-$key" use_index "$i"
        hc keybind "$Mod-Shift-$key" move_index "$i"
    fi
done

# layouting
hc keybind $Mod-q remove
hc keybind $Mod-m fullscreen toggle
hc keybind $Mod-period pseudotile toggle
hc keybind $Mod-comma floating toggle
# The following cycles through the available layouts within a frame, but skips
# layouts, if the layout change wouldn't affect the actual window positions.
# I.e. if there are two windows within a frame, the grid layout is skipped.
hc keybind $Mod-o                                                           \
            or , and . compare tags.focus.curframe_wcount = 2                   \
                     . cycle_layout +1 vertical horizontal max vertical grid    \
               , cycle_layout +1

# mouse
hc mouseunbind --all
hc mousebind $Mod-Button1 move
hc mousebind $Mod-Button2 zoom
hc mousebind $Mod-Button3 resize

hc set update_dragged_clients 1

# focus
hc keybind $Mod-Tab         cycle_all +1
hc keybind $Mod-Shift-Tab   cycle_all -1
hc keybind $Mod-space cycle
hc keybind $Mod-i jumpto urgent

# theme
hc attr theme.tiling.reset 1
hc attr theme.floating.reset 1

hc set frame_border_width 0
hc set always_show_frame 0
hc set frame_bg_transparent 1
hc set frame_transparent_width 0

hc set frame_gap 6

# NeXT
# hc attr theme.active.color '#000000'
# hc attr theme.normal.color '#b8b8b8'
# hc attr theme.urgent.color '#cb4b16'

# CDE
# hc attr theme.active.color '#eeaa71'
# hc attr theme.normal.color '#9a9a9a'
# hc attr theme.urgent.color '#cb4b16'

# solarized
hc attr theme.active.color '#586e75'
hc attr theme.normal.color '#C0C0C0'
hc attr theme.urgent.color '#cb4b16'

hc attr theme.inner_width 0
hc attr theme.border_width 4

hc attr theme.floating.border_width 4
hc attr theme.floating.outer_width 0

hc set window_gap 0
hc set frame_padding 0

hc set smart_window_surroundings 0
hc set smart_frame_surroundings 1
hc set mouse_recenter_gap 0
hc set focus_follows_mouse 1

# rules
hc unrule -F
hc rule focus=on # normally focus new clients
hc rule windowtype~'_NET_WM_WINDOW_TYPE_(DIALOG|UTILITY|SPLASH)' pseudotile=on
hc rule windowtype='_NET_WM_WINDOW_TYPE_DIALOG' focus=on
hc rule windowtype~'_NET_WM_WINDOW_TYPE_(NOTIFICATION|DOCK|DESKTOP)' manage=off

hc set tree_style '╾│ ├└╼─┐'

# unlock, just to be sure
hc unlock

# app shortcuts
hc keybind $Mod-f spawn hl-jump.sh class Firefox firefox
hc keybind $Mod-g spawn hl-jump.sh class Telegram telegram-desktop
hc keybind $Mod-n spawn hl-jump.sh class org.gnome.Nautilus nautilus
hc keybind $Mod-m spawn hl-jump.sh class Mousepad mousepad

# layout shortcuts
hc keybind $Mod-Shift-c load "(split horizontal:0.250000:0 (clients max:0 0x800005) (split horizontal:0.6666666:0 (clients max:0 0x1000003) (clients max:0 0x1400005)))"

