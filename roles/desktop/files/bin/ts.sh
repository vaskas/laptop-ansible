#!/bin/sh

text="$(xsel -o -b)"

telegram-send "$text" && notify-send "Sent $text to Telegram"
