#!/usr/bin/env bash

#
# ape2flac.sh
#

function f2m(){
        FILE=$(echo "$1" | perl -p -e 's/.ape$//g');
        if [ ! -f "$FILE".flac ] ; then
                ffmpeg -v quiet -i "$FILE.ape" "$FILE.flac"
        fi
}
export -f f2m
find . -name '*.ape' | xargs -I {} -P $(nproc) bash -c 'f2m "$@"' _ "{}"
