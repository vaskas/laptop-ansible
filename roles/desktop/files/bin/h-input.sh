#!/bin/sh

# higher key repeat rate
xset r rate 350 60

setxkbmap -layout "us,ru" -option "grp:shifts_toggle,grp_led:scroll"
xmodmap $HOME/.Xmodmap

# enable touchpad when typing
# xinput set-prop 'SYNA2B34:00 06CB:7F8D Touchpad' "libinput Disable While Typing Enabled" 0
