echo -n "Melbourne: " && TZ=Australia/Melbourne date
echo -n "Los Angeles: " && TZ=America/Los_Angeles date
echo -n "London: " && TZ=Europe/London date
echo -n "Honolulu: " && TZ=Pacific/Honolulu date
echo -n "Moscow: " && TZ=Europe/Moscow date
echo -n "Tomsk: " && TZ=Asia/Tomsk date

