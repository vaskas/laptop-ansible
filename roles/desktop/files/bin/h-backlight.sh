#!/bin/bash
set -e

sudo chmod a+w /sys/class/backlight/intel_backlight/brightness

file="/sys/class/backlight/intel_backlight/brightness"
current=$(cat "$file")

max=$(cat "/sys/class/backlight/intel_backlight/max_brightness")

new="$current"

if [ "$1" = "-get" ]; then
  echo "scale=2;$current/$max * 100" | bc -l
  exit 0
fi

if [ "$1" = "-inc" ]; then
  new=$(( current + $2 ))
fi

if [ "$1" = "-dec" ]; then
  new=$(( current - $2 ))
fi
echo "$new" | tee "$file"
