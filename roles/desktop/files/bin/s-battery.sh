#!/bin/sh

level=`acpi | cut -d " " -f 4 | sed 's/[^0-9]*//g'`
icon=""

if ! acpi -b | grep -q Charging; then
  icon=""
fi

echo "$icon $level%"
