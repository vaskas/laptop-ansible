#!/bin/sh

if xrandr | grep "^HDMI1 connected" | grep "+1920+0" ; then
  xrandr --output HDMI1 --off
else
  xrandr --output HDMI1 --mode 1440x900 --pos 1920x0
fi

$HOME/.bin/h-input.sh

herbstclient detect_monitors

