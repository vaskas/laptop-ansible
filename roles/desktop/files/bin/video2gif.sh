#!/bin/sh

ffmpeg -y -i "$1" \
-vf fps=10,scale=320:-1:flags=lanczos,palettegen "$1"_palette.png

ffmpeg -i "$1" -i "$1"_palette.png -filter_complex \
"fps=8,scale=1362:-1:flags=lanczos[x];[x][1:v]paletteuse" "$1".gif

