#!/bin/bash

ismute=`pamixer --get-mute`

if [ $ismute = "true" ]
then
    # mute
    muteicon=""
    echo "$muteicon"
else
    # not mute
    volume=`pamixer --get-volume`
    icon=""
    echo "$icon $volume"
fi
