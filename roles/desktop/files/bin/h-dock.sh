#!/bin/sh

# this="LVDS1"
this="eDP1"
# this="eDP-1"

ext="DP1"
# ext="DP-1"
# ext="HDMI1"

# mode="1920x1080"
mode="2560x1440"
# mode="3840x2160"

if xrandr | grep "^$this connected" | grep "+0+0" ; then
  xrandr --output $ext --mode $mode --pos 0x0 --output $this --off
else
  xrandr --output $ext --off --output $this --auto
fi

$HOME/.bin/h-input.sh

herbstclient detect_monitors
