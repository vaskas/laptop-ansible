#!/bin/sh
echo "Usage: flacsplit.sh file.cue file.flac"

cuebreakpoints "$1" | shnsplit -o flac "$2"
cuetag.sh "$1" split-track*.flac
