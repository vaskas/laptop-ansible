#!/bin/sh
net="$(nmcli | grep "connected to" | grep -v docker | grep -v mullvad | awk -F "connected to" '{print $2}')"

vpn_icon=""

if ip addr | grep -q mullvad; then
  vpn_icon="  "
fi

echo "$net$vpn_icon"
