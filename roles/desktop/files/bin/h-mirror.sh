#!/bin/sh

if xrandr | grep "^HDMI1 connected" | grep "+0+0" ; then
  xrandr --output HDMI1 --off
else
  xrandr --output HDMI1 --mode 1920x1080
fi

$HOME/.bin/h-input.sh

herbstclient detect_monitors

